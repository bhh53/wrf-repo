#bhh53 Fall 2020
#Precipitable Water & MSLP Plot

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.colors as mcolors
#from matplotlib.cm import get_cmap
#import metpy.plots.ctables as ctables
#import metpy.calc as mcalc
import cartopy.crs as ccrs
import cartopy.feature as cfeature
#import cartopy.mpl.ticker as cticker
#import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()

times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

####################
idxlat = 42.4534
idxlon = -76.4735
locname = 'Ithaca'
####################

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(idxlon, idxlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
alltimes = wrf.ALL_TIMES
#alltimes = 0

ncfile = nc.Dataset(root + output)

for i in range(len(times)):
    print('Fetching variables...')
    alltimes = i
    pwvar = wrf.getvar(ncfile, 'pw', timeidx = alltimes)
    print('PW Variable fetched.')
    slpvar = wrf.getvar(ncfile, 'slp', timeidx = alltimes)
    print('SLP Variable fetched.')
    #slp_smooth = ndimage.gaussian_filter(slpvar, sigma = 3)
    slplevs = np.arange(900, 1100, 4)
    pwlevs = np.arange(0, 80, 5)
    def plot_pw(pwvar, slpvar, time=0):
        fig = plt.figure(figsize = (12,8), dpi = 200)
        timevals = str(times[time])
        tim = timevals[:-10]
        proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
        dataproj = ccrs.PlateCarree()
        ax = plt.subplot(111, projection = proj)
        ax.set_extent([-124, -60, 16, 62]) #CONUS
        ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth = 0.75)
        ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none')
        #ax.add_feature(cfeature.RIVERS.with_scale('10m'), edgecolor = cfeature.COLORS['water'])
        ax.add_feature(states, linewidth=.5, edgecolor='black')
        pw_fconts = plt.contourf(lon, lat, pwvar, levels = pwlevs, cmap = 'ocean_r',extend = 'max', transform = dataproj)
        cbar = plt.colorbar(pw_fconts, orientation = 'vertical', ticks = pwlevs, pad = 0.00)
        cbar.set_label('Precipitable Water (mm)')
        ax.set_aspect('auto')
        slp_smooth = ndimage.gaussian_filter(slpvar, sigma = 3)
        slpconts = plt.contour(lon, lat, slp_smooth, levels = slplevs, colors = 'darkgrey', linewidths = 1, transform = dataproj)
        slplabels = plt.clabel(slpconts, colors = 'black', fontsize = 8, inline = 1, fmt = '%4.0f')
        plt.suptitle('Precipitable Water (mm) and SLP (hPa)', fontsize = 16)
        plt.title('Valid: ' + tim + ' Z', fontsize = 12, loc = 'left')
        plt.title('Initialized: ' + init + ' Z', fontsize = 12, loc = 'right')
        plt.savefig('pw_amd_' + init + '_' + str(time) + '.png')
        plt.clf()
        return ax
    
    #i = alltimes
    plot_pw(pwvar, slpvar, time = i)
    print('Plot ' + str(i) + ' Generated.')
print('Done.')
