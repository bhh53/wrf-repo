#bhh53 Summer 2020
#WRF Domain Boundaries Plot

import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import netCDF4 as nc
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import wrf

root = '/data/bhh53/wrfout/'
output = 'wrfout_d01_2020-07-14_12:00:00'
#data = xr.open_dataset(root + output)


def get_plot_element(infile):
    rootgroup = nc.Dataset(infile, 'r')
    p = wrf.getvar(rootgroup, 'RAINNC')
    #lats, lons = wrf.latlon_coords(p)
    cart_proj = wrf.get_cartopy(p)
    xlim = wrf.cartopy_xlim(p)
    ylim = wrf.cartopy_ylim(p)
    rootgroup.close()
    return cart_proj, xlim, ylim

file_d01 = root + output
cart_proj, xlim_d01, ylim_d01 = get_plot_element(file_d01)

fig = plt.figure(figsize=(10,8), dpi = 200)
ax = plt.axes(projection=cart_proj)

states = cfeature.NaturalEarthFeature(category="cultural", scale="10m", facecolor="none", name="admin_1_states_provinces_shp")
ax.add_feature(cfeature.COASTLINE.with_scale('50m'), linewidth = .8, edgecolor = 'black')
ax.add_feature(cfeature.LAKES.with_scale('10m'), edgecolor = 'black', facecolor = cfeature.COLORS['water'])
ax.add_feature(states, linewidth=.5, edgecolor='black')

# d01
ax.set_xlim([xlim_d01[0]-(xlim_d01[1]-xlim_d01[0])/15, xlim_d01[1]+(xlim_d01[1]-xlim_d01[0])/15])
ax.set_ylim([ylim_d01[0]-(ylim_d01[1]-ylim_d01[0])/15, ylim_d01[1]+(ylim_d01[1]-ylim_d01[0])/15])

# d01 box
ax.add_patch(mpl.patches.Rectangle((xlim_d01[0], ylim_d01[0]), xlim_d01[1]-xlim_d01[0], ylim_d01[1]-ylim_d01[0], fill=None, lw=3, edgecolor='blue', zorder=10))
ax.text(xlim_d01[0]+(xlim_d01[1]-xlim_d01[0])*0.05, ylim_d01[0]+(ylim_d01[1]-ylim_d01[0])*0.9, 'D01', size=15, color='blue', zorder=10)

ax.set_title('WRF Domain Setup', size=20)
fig.savefig('domainplot_unnested_10km.png')

print('Plot Generated.')




