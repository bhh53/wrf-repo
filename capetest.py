#bhh53 Fall 2020
#CAPE Plot

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.cm import get_cmap
import metpy.plots.ctables as ctables
import metpy.calc as mcalc
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()
times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

####################
idxlat = 42.4534
idxlon = -76.4735
locname = 'Ithaca'
####################


def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(idxlon, idxlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
print('Fetching variables...')
#alltimes = wrf.ALL_TIMES
alltimes = 24

ncfile = nc.Dataset(root + output)
#print(ncfile)
pvar = wrf.getvar(ncfile, 'pressure', timeidx = alltimes)
print('Pressure fetched.')
psum = np.sum(pvar)
#print(np.isnan(psum))
tkvar = wrf.getvar(ncfile, 'tk', timeidx = alltimes)
print('Temperature fetched.')
tsum = np.sum(tkvar)
#print(np.isnan(tsum))
qvvar = xr.DataArray(data.QVAPOR[alltimes])
#qv = qvvar[alltimes]
#print(qvvar)
print('QVAPOR fetched.')
qsum = np.sum(qvvar)
#print(np.isnan(qsum))
zvar = wrf.getvar(ncfile, 'height', units = 'm', timeidx = alltimes)
print('Geopotential height fetched.')
zsum = np.sum(zvar)
#print(np.isnan(zsum))
tervar = wrf.getvar(ncfile, 'ter', units = 'm', timeidx = alltimes)
print('Terrain height fetched.')
tersum = np.sum(tervar)
#print(np.isnan(tersum))
psfcvar = xr.DataArray(data.PSFC[alltimes]) / 100.
#psfc = psfcvar[alltimes] / 100. #convert to hPa
#print(psfc)
print('PSFC fetched.')
pssum = np.sum(psfcvar)
#print(np.isnan(pssum))

print('All Variables Fetched.')
#cape_2didx = wrf.cape_2d(pvar[:, idx[0], idx[1]], tkvar[:, idx[0], idx[1]], qvvar[:,idx[0], idx[1]], zvar[:, idx[0], idx[1]], tervar[idx[0], idx[1]], psfcvar[idx[0], idx[1]], ter_follow = True)
#print(cape_2didx)
cape_2d = wrf.cape_2d(pvar,tkvar,qvvar,zvar,tervar,psfcvar,ter_follow=True)
cape = cape_2d[0].values
cin = cape_2d[1].values
lcl = cape_2d[2].values
lfc = cape_2d[3].values
#print(cape)
#print(cin)
#print(lcl)
#print(lfc)
print(np.nanmax(cape))
print(np.nanmax(cin))
print(lcl.max())
print(lfc.max())
print('Beginning Plotting...')

def plot_background(ax):
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=.8)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    return ax


capelevs = [0, 50, 100, 175, 250, 375, 500, 750, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 5000]
#capenorm = mcolors.BoundaryNorm(capelevs, 16)
capenorm = mcolors.DivergingNorm(vmin = 0., vcenter = 500., vmax = 5000.)
def plot_cape(cape, time = 0):
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    timevals = str(times[time])
    tim = timevals[:-10]
    fig = plt.figure(figsize = (12,8), dpi = 300)
    ax = plt.subplot(111, projection = proj)
    plot_background(ax)
    cape_fconts = plt.contourf(lon, lat, cape, levels = capelevs, cmap = 'twilight', norm = capenorm, transform = dataproj)
    cbar = plt.colorbar(cape_fconts, ax=ax, orientation = 'vertical', ticks = capelevs, pad = 0.00)
    cbar.set_label('Maximum CAPE (J $kg^{-1}$)')
    ax.set_aspect('auto')
    plt.title('Maximum Convective Available\nPotential Energy (MCAPE)', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + tim + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('cape_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_cape(cape, time = i)
print('CAPE Plot generated.')
cinlevs = np.arange(0, 750, 10)
cinnorm = mcolors.DivergingNorm(vmin = 0., vcenter = 200., vmax = 750.)
def plot_cin(cin, time = 0):
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    timevals = str(times[time])
    tim = timevals[:-10]
    fig = plt.figure(figsize = (12,8), dpi = 300)
    ax = plt.subplot(111, projection = proj)
    plot_background(ax)
    cin_fconts = plt.contourf(lon, lat, cin, levels = cinlevs, cmap = 'magma_r', norm = cinnorm, transform = dataproj)
    cbar = plt.colorbar(cin_fconts, ax=ax, orientation = 'vertical', ticks = np.arange(0, 750, 50), pad = 0.00)
    cbar.set_label('Maximum CIN (J $kg^{-1}$)')
    ax.set_aspect('auto')
    plt.title('Maximum Convective Inhibition (MCIN)', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + tim + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('cin_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

plot_cin(cin, time = i)
print('CIN Plot generated.')

lcllevs = np.arange(0., 15000., 200.)

def plot_lcl(lcl, time = 0):
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    timevals = str(times[time])
    tim = timevals[:-10]
    fig = plt.figure(figsize = (12,8), dpi = 200)
    ax = plt.subplot(111, projection = proj)
    plot_background(ax)
    lcl_fconts = plt.contourf(lon, lat, lcl, levels = lcllevs, cmap = 'terrain', transform = dataproj)
    cbar = plt.colorbar(lcl_fconts, ax=ax, orientation = 'vertical', ticks = np.arange(0, 15000, 1000), pad = 0.00)
    cbar.set_label('LCL Height (m)')
    ax.set_aspect('auto')
    plt.title('Lifted Condensation\nLevel (LCL) Height (m)', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + tim + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('lcl_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

plot_lcl(lcl, time = i)
print('LCL Plot generated.')


print('Done.')


