#bhh53 Fall 2020
#CAPE Plot

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.cm import get_cmap
import metpy.plots.ctables as ctables
import metpy.calc as mcalc
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()

times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

####################
idxlat = 42.4534
idxlon = -76.4735
locname = 'Ithaca'
####################

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(idxlon, idxlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
print('Fetching variables...')
#alltimes = wrf.ALL_TIMES
alltimes = 0

ncfile = nc.Dataset(root + output)
#print(ncfile)
rh2var = wrf.getvar(ncfile, 'rh2', timeidx = alltimes)
print('2m RH fetched.')

rh2 = rh2var.values

def plot_background(ax):
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=.8)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    return ax

rhlevs = np.arange(0,105,5)
def plot_rh2(rh2, time = 0):
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    timevals = str(times[time])
    tim = timevals[:-10]
    fig = plt.figure(figsize = (12,8), dpi = 300)
    ax = plt.subplot(111, projection = proj)
    plot_background(ax)
    rh2_fconts = plt.contourf(lon, lat, rh2, levels = rhlevs, cmap = 'gist_earth_r', transform = dataproj)
    cbar = plt.colorbar(rh2_fconts, orientation = 'vertical', ticks = rhlevs, pad = 0.00)
    cbar.set_label('Relative Humidity (%)')
    ax.set_aspect('auto')
    plt.title('Relative Humidity (%) at 2 meters', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + tim + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('rh2_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_rh2(rh2, time = i)

print('Done.')

