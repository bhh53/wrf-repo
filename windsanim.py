#bhh53 Summer 2020
#Winds Plot loop

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.cm import get_cmap
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values


ithlat = 42.4534
ithlon = -76.4735
mialat = 25.79
mialon = -80.32

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
print('Fetching variables...')
#alltimes = wrf.ALL_TIMES
alltimes = 48

ncfile = nc.Dataset(root + output)
pvar = wrf.getvar(ncfile, 'pressure', timeidx = alltimes)
print('Pressure fetched.')
zvar = wrf.getvar(ncfile, 'z', units = 'dm', timeidx = alltimes)
print('Z fetched')
uavar = wrf.getvar(ncfile, 'ua', units = 'kt', timeidx = alltimes)
print('U fetched')
vavar = wrf.getvar(ncfile, 'va', units = 'kt', timeidx = alltimes)
print('V fetched')
wspdvar = wrf.getvar(ncfile, 'wspd_wdir', units='kts', timeidx = alltimes)[0,:]
print('WSPD fetched')
wdirvar = wrf.getvar(ncfile, 'wspd_wdir', units = 'kts', timeidx = alltimes)[1,:]
print('Fetched all variables.')

p = xr.DataArray(pvar).values
z = xr.DataArray(zvar).values
u = xr.DataArray(uavar).values
v = xr.DataArray(vavar).values
wspd = xr.DataArray(wspdvar).values
wdir = xr.DataArray(wdirvar).values
print('Arrays Ready.')

####################
## Pressure Level ##
plevel = 200
####################

ht_z = wrf.interplevel(z, p, plevel).values
ht_smooth = ndimage.gaussian_filter(ht_z, sigma = 3)
u_z = wrf.interplevel(u, p, plevel).values
v_z = wrf.interplevel(v, p, plevel).values
wspd_z = wrf.interplevel(wspd, p, plevel).values
wdir_z = wrf.interplevel(wdir, p, plevel).values
print('Completed interpolations. Beginning Plots.')
print('ht max = ',np.nanmax(ht_z))
print('ht min = ',np.nanmin(ht_z))


def plot_uv_z(u_z, v_z, ht_smooth, wspd_z, wdir_z, plevel = plevel, time = 0):
    fig = plt.figure(figsize = (12,8), dpi = 300)
    timevals = str(times[time])
    tim = timevals[:-10]
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    ax = plt.subplot(111, projection = proj)
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    idxwspd = wspd_z[idx[0], idx[1]] # Ithaca wind speed
    idxwdir = wdir_z[idx[0], idx[1]] # Ithaca wind dir
    if plevel <= 300:
        zlevs = np.arange(780, 1680, 12)
        windlevs = np.arange(50, 160, 10)
    elif 300 < plevel <= 500:
        zlevs = np.arange(420, 780, 6)
        windlevs = np.arange(50, 130, 10)
    elif plevel > 500:
        zlevs = np.arange(0, 345, 3)
        windlevs = np.arange(30, 130, 10)
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=1)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    wspd_conts = plt.contourf(lon, lat, wspd_z, levels = windlevs, cmap = get_cmap('rainbow'), extend = 'max', transform = dataproj)
    cbar = plt.colorbar(wspd_conts, ax=ax, orientation = 'vertical', ticks = windlevs, extend = 'max', pad = 0.01)
    cbar.set_label('Wind Speed (kt)')
    ax.set_aspect('auto')
    barbs = plt.barbs(lon[::20, ::20], lat[::20, ::20], u_z[::20, ::20], v_z[::20, ::20], length=5, color = 'darkgrey', transform = dataproj)
    zconts = plt.contour(lon, lat, ht_smooth, levels = zlevs, colors = 'k', transform = dataproj, linewidths = 1)
    zlabel = plt.clabel(zconts, fontsize = 10, inline = 1, fmt = '%i')
    ax.text(0,-.01, 'Ithaca Wind: ' + str(int(idxwspd)) + ' kt @ ' + str(int(idxwdir)) + ' degrees', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'left')
    ax.text(1,-.01, 'Max Wind Speed: ' + str(int(np.nanmax(wspd_z))) + ' kt', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'right')
    plt.title(str(plevel) + ' mb Height (dam),\nWind Speed (kt), Barbs (kt)', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + str(tim) + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('winds_' + str(plevel) + '_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

#for i in range(len(times)):
i = alltimes
plot_uv_z(u_z, v_z, ht_z, wspd_z, wdir_z, plevel = plevel, time = i)
print('Plot ' + str(i) + ' Generated.')


print('Done.')
