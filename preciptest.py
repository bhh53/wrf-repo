#bhh53 Summer 2020
#Precip

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc

root = '/data/bhh53/wrfout/'
output = 'wrfout_d01_2020-07-14_12:00:00'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME)
inittime = str(times[0].values)
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

ithlat = 42.4534
ithlon = -76.4735
def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)

rainc = data.RAINC
rainnc = data.RAINNC
cda = xr.DataArray(rainc)
ncda = xr.DataArray(rainnc)

precip = cda.values + ncda.values
smoothpre = wrf.smooth2d(precip, 4, cenweight=2)

proj = ccrs.PlateCarree()
states = cfeature.NaturalEarthFeature(category="cultural", scale="10m", facecolor="none", name="admin_1_states_provinces_shp")

preciplevs = [0, 1, 2.5, 5, 7.5, 10, 15, 20, 30, 40, 50, 70, 100, 150, 200, 250, 300, 400, 500, 600, 750]

cmap_data = [(1.0,1.0,1.0),(0.3137255012989044, 0.8156862854957581, 0.8156862854957581), (0.0, 1.0, 1.0),(0.0, 0.8784313797950745, 0.501960813999176),(0.0, 0.7529411911964417, 0.0),(0.501960813999176, 0.8784313797950745, 0.0), (1.0, 1.0, 0.0),(1.0, 0.6274510025978088, 0.0),(1.0, 0.0, 0.0),(1.0, 0.125490203499794, 0.501960813999176),(0.9411764740943909, 0.250980406999588, 1.0),(0.501960813999176, 0.125490203499794, 1.0), (0.250980406999588, 0.250980406999588, 1.0),(0.125490203499794, 0.125490203499794, 0.501960813999176), (0.125490203499794, 0.125490203499794, 0.125490203499794), (0.501960813999176, 0.501960813999176, 0.501960813999176),(0.8784313797950745, 0.8784313797950745, 0.8784313797950745),(0.9333333373069763, 0.8313725590705872, 0.7372549176216125),(0.8549019694328308, 0.6509804129600525, 0.47058823704719543),(0.6274510025978088, 0.42352941632270813, 0.23529411852359772),(0.4000000059604645, 0.20000000298023224, 0.0)]

cmap = mcolors.ListedColormap(cmap_data, 'precipitation')
norm = mcolors.BoundaryNorm(preciplevs, 21) 

#norm, cmap = ctables.registry.get_with_boundaries('precipitation', preciplevs)


#print(len(preciplevs), len(cmap_data))

def plot_precip(smoothpre, time = 3):
    fig = plt.figure(figsize = (16,10), dpi = 200)
    timevals = str(times[time].values)
    tim = timevals[:-10]
    precipvals = smoothpre[i][idx[0], idx[1]]
    preciplevs = [0, 1, 2.5, 5, 7.5, 10, 15, 20, 30, 40, 50, 70, 100, 150, 200, 250, 300, 400, 500, 600, 750]
    ax = plt.axes(projection = proj)
    ax.set_extent([-82, -66.8, 38, 47.7])
    ax.add_feature(cfeature.COASTLINE.with_scale('10m'), edgecolor = 'black')
    ax.add_feature(cfeature.LAKES.with_scale('10m'), edgecolor = 'black', facecolor = 'none')
    ax.add_feature(cfeature.RIVERS.with_scale('10m'), edgecolor = cfeature.COLORS['water'])
    ax.add_feature(states, linewidth=1, edgecolor='black')
    precips = ax.contourf(lon, lat, smoothpre[time], levels = preciplevs, cmap = cmap, norm = norm, extend = 'neither')
    #precipcont = ax.contour(lon, lat, precip[i], levels = preciplevs, linewidths = 2, colors = 'black')
    cbar = plt.colorbar(precips, orientation = 'vertical', ticks = preciplevs, pad = 0.00)
    cbar.set_label('Rain Since ' + init + ' (mm)')
    ax.set_aspect('auto')
    ax.text(0,-.01, 'Ithaca Rain Total: ' + str(int(precipvals)) + ' mm', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'left')
    ax.text(1,-.01, 'Max Rain Total: ' + str(int(smoothpre[time].max())) + ' mm', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'right')
    plt.title('Total Accumulated Precipitation', fontsize = 16)
    plt.title('Initialized: ' + init + ' Z\nValid: ' + tim + ' Z', fontsize = 12, loc = 'right')
    plt.savefig('precip_' + str(time) + '.png')
    #print('Precip Plot ' + time + 'Generated.')
    plt.clf()
    return ax

for i in range(len(times)):
    plot_precip(precip, time = i)
    print('Precip Plot ' + str(i) + ' Generated.')
print('Done.')

