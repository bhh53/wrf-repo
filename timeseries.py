##bhh53 Summer 2020
##test time series of T2/Q2
from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc

root = '/data/bhh53/wrfout/'
output = 'wrfout_d01_2020-07-08_12:00:00'
data = xr.open_dataset(root + output)
data.info()

lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values
ithlat = 42.4534
ithlon = -76.4735
clelat = 41.47
clelon = -81.54


locname = 'Ithaca'

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)

times = xr.DataArray(data.XTIME)
inittime = str(times[0].values)
init = inittime[:-10]

temps = data.T2
T2 = xr.DataArray(temps)
#T2 = wrf.smooth2d(xr.DataArray(temps), 3, cenweight=2)
T2C = T2 - 273.15
T2F = T2C * 9/5. + 32.
#print('Max T2 is:', T2F.values.max())
#print('Min T2 is:', T2F.values.min())
hums = data.Q2
#Q2 = wrf.smooth2d(xr.DataArray(hums), 3, cenweight=2)
Q2 = xr.DataArray(hums)
Q2g = Q2 * 1e3
#print(T2)
#print(Q2)
#print('Max Q2 is:', Q2.values.max())
#print('Min Q2 is:', Q2.values.min())

T2Flist = []
Q2list = []
print(idx[0])
print([idx[0], idx[1]])


for i in range(len(times)):
    timevals = str(times[i].values)
    #print(timevals)
    time = timevals[:-10]
    ptT2F = T2F[i][idx[0], idx[1]].values #T2 at certain gridpoint idx
    ptQ2 = Q2g[i][idx[0], idx[1]].values
    #print('Ithaca T is:', ithT2F)
    #print('Ithaca Q is:', ithQ2)
    T2Flist.append(ptT2F)
    Q2list.append(ptQ2)

#fig, (axT, axQ) = plt.subplots(nrows=2, figsize=(4,8), sharex = 'col', dpi=200)
fig = plt.figure(figsize = (6,10), dpi = 200)
#ax = fig.add_subplot(111)
axT = fig.add_subplot(211)
axQ = fig.add_subplot(212)

#ax.spines['top'].set_color('none')
#ax.spines['bottom'].set_color('none')
#ax.spines['left'].set_color('none')
#ax.spines['right'].set_color('none')
#ax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

times_ = []
timelist = times.values
for t in range(len(timelist)):
    timestr = str(timelist[t])
    tim = timestr[5:-10]
    print(tim)
    times_.append(tim)
print(times_)
#timelabels = timelist[:-10]
#print(timelabels)
print(timelist)
for i in range(len(times)):
    time = str(times[i].values)
    #timelabels = time[:-10]
    Tplot = axT.plot(timelist, T2Flist, color = 'black', lw = 2)
    Qplot = axQ.plot(timelist, Q2list, color = 'green', lw = 2)
axT.set_title(locname + ' Temperature Forecast', loc = 'center')
axQ.set_title(locname + ' Specific Humidity Forecast', loc = 'center')
axT.set_ylabel('2-Meter Temperature (' + u'\N{DEGREE SIGN}' + 'F)')
axQ.set_ylabel('2-Meter Specific Humidity (g/kg)')

#timelabels = ['18 Z','','00 Z','','06 Z','','12 Z',''] * 3 #len(times)/8
#print(timelabels)
#plt.title('Initialized: ' + init + '\nValid: ' + str(time), fontsize = 12, loc = 'right')
#plt.xticks(timelabels)#, rotation = 'vertical', fontsize = 10)
#plt.margins(0.9)
Tlabels = np.arange(40, 100, 5)
axT.set_yticks(Tlabels)
Qlabels = np.arange(0,24,2)
axQ.set_yticks(Qlabels)

#timelabels = 
#axt.set_xticks(

plt.savefig('timeseries_'+locname+'.png')    


print(locname + ' time series generated.')
