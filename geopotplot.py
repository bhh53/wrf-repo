#bhh53 Summer 2020
#WRF Geopotential Plot 

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc

root = '/data/bhh53/wrfout/'
output = 'wrfout_d01_2020-07-14_12:00:00'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME)
inittime = str(times[0].values)
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values


ithlat = 42.4534
ithlon = -76.4735
def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")


ncfile = nc.Dataset(root + output)
p = wrf.getvar(ncfile, 'pressure')
ht = wrf.getvar(ncfile, 'z')
ht_500 = wrf.interplevel(ht, p, 500.0)
z500 = xr.DataArray(ht_500)
ht_300 = wrf.interplevel(ht, p, 300.0)


fig = plt.figure(figsize = (20,12), dpi = 200)
pda = xr.DataArray(wrf.getvar(ncfile, 'pressure'))
ht = wrf.getvar(ncfile, 'z')
z = xr.DataArray(ht)
ax = plt.axes(projection = ccrs.PlateCarree())


def plot_z(z, p = 500, time = 0):
    fig = plt.figure(figsize = (20,12), dpi = 200)
    timeval = str(times[time].values)
    tim = timeval[:-10]
    pda = xr.DataArray(wrf.getvar(ncfile, 'pressure'))
    geopt = wrf.getvar(ncfile, 'geopt')
    ht = wrf.getvar(ncfile, 'height')
    z = xr.DataArray(geopt) - xr.DataArray(ht)
    print(z[0])
    z_p = xr.DataArray(wrf.interplevel(z, pda, p))
    print(z_p)
    if p <= 300:
        zlevs = np.arange(0, 30000, 1200)
    elif 300 < p <= 500:
        zlevs = np.arange(0, 30000, 600)
    elif p > 500:
        zlevs = np.arange(0, 30000, 300)
    ax = plt.axes(projection = ccrs.PlateCarree())
    ax.set_extent([-82, -71, 38, 46])
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black')
    ax.add_feature(cfeature.LAKES.with_scale('10m'), edgecolor = 'black', facecolor = 'none')
    ax.add_feature(states, linewidth=1, edgecolor='black')
    zs = ax.contour(lon, lat, z_p, levels = zlevs, linewidths = 1, colors = 'blue')
    zlabel = plt.clabel(zs, fontsize = 12, inline=1, fmt='%5.0f', colors = 'blue')
    plt.title('Geopotential Height (m)', fontsize = 20)
    plt.title('Initialized: ' + init + ' Z\nValid: ' + str(tim) + ' Z', fontsize = 12, loc = 'right')
    plt.savefig('Z_plot_' + str(time) + '.png')
    plt.clf()
    return ax

plot_z(z=z, p = 850., time = 0) 
plot_z(z=z, p = 925., time = 1)
print('End.')
