## bhh53 Fall 2020
#Cloud Fraction Plot

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.cm import ScalarMappable
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values


ithlat = 42.4534
ithlon = -76.4735

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
print('Fetching variables...')
#alltimes = wrf.ALL_TIMES
alltimes = 24

ncfile = nc.Dataset(root + output)
cfvar = wrf.getvar(ncfile, 'cloudfrac', timeidx = alltimes)
cf_low = 100 * cfvar[0].values
cf_mid = 100 * cfvar[1].values
cf_high = 100 * cfvar[2].values
cf_total = 100 * np.amax(cfvar, axis = 0)
#print(cfvar)
#print('---------')
#print(cf_total)
#idxcf = cfvar[idx[0], idx[1]] #Ithaca Cloud Fraction
#pwvar = wrf.getvar(ncfile, 'pw', timeidx = alltimes)
print('All Variables Fetched. Beginning Plotting...')
def plot_background(ax):
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=1)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    return ax
def plot_cloudfrac(cf_low, cf_mid, cf_high, cf_total, time = 0):
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    timevals = str(times[time])
    tim = timevals[:-10]
    fig, axarr = plt.subplots(nrows = 2, ncols = 2, figsize = (24,16), constrained_layout = False, subplot_kw={'projection': proj})
    axlist = axarr.flatten()
    print(axlist)
    for ax in axlist:
        plot_background(ax)
        print('Axes Background generated.')
    #cflevs = np.arange(.05,1.05,.05)
    #cfcontlevs = np.arange(0,1.20,.20)
    #cf1 = axlist[0].contourf(lon, lat, cf_low, levels = cflevs, cmap = 'bone_r', transform = dataproj)
    c1 = axlist[0].pcolormesh(lon, lat, cf_low, vmin = 5, vmax = 100, cmap = 'bone_r', transform = dataproj)
    axlist[0].set_title('LOW Cloud Fraction (%)', fontsize = 16)
    #cf2 = axlist[1].contourf(lon, lat, cf_mid, levels = cflevs, cmap = 'bone_r', transform = dataproj)
    print('Axes 1 Completed.')
    c2 = axlist[1].pcolormesh(lon, lat, cf_mid, vmin = 5, vmax = 100, cmap = 'bone_r', transform = dataproj)
    axlist[1].set_title('MID Cloud Fraction (%)', fontsize = 16)
    print('Axes 2 Completed.')
    #cf3 = axlist[2].contourf(lon, lat, cf_high, levels = cflevs, cmap = 'bone_r', transform = dataproj)
    c3 = axlist[2].pcolormesh(lon, lat, cf_high, vmin = 5, vmax = 100, cmap = 'bone_r', transform = dataproj)
    print('Axes 3 Completed.')
    axlist[2].set_title('HIGH Cloud Fraction (%)', fontsize = 16)
    #cf4 = axlist[3].contourf(lon, lat, cf_total, levels = cflevs, cmap = 'bone_r', transform = dataproj)
    c4 = axlist[3].pcolormesh(lon, lat, cf_total, vmin = 5, vmax = 100, cmap = 'bone_r', transform = dataproj)
    axlist[3].set_title('TOTAL Cloud Fraction (%)', fontsize = 16)
    print('Axes 4 Completed.')
    #cbar = plt.colorbar()
    #cf_labels = plt.clabel(cf_conts, fontsize = 10, inline = 1, fmt = '%i')
    #ax.text(0,-.01, 'Ithaca Cloud Fraction: ' + str(int(idxcf)) + ' %', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'left')
    #fig.colorbar(cf_total, ax=axlist[:], location = 'right', shrink = 0.8)
    plt.colorbar(ScalarMappable(norm = None, cmap = 'bone_r'), ax = axarr.ravel().tolist(), shrink = .8)
    fig.suptitle('Cloud Fraction Valid: ' + str(tim) + ' Z', fontsize = 24)
    plt.savefig('cfrac_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_cloudfrac(cf_low, cf_mid, cf_high, cf_total, time = i)
print('Plot ' + str(i) + ' Generated.')

print('Done.')


