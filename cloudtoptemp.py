#bhh53 Fall 2020
#Cloud Top Temperature w/ Color Enhancement Plot

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
#from matplotlib.cm import get_cmap
#import metpy.plots.ctables as ctables
#import metpy.calc as mcalc
import cartopy.crs as ccrs
import cartopy.feature as cfeature
#import cartopy.mpl.ticker as cticker
#import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()

times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

####################
idxlat = 42.4534
idxlon = -76.4735
locname = 'Ithaca'
####################

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(idxlon, idxlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
dataproj = ccrs.PlateCarree()
#alltimes = wrf.ALL_TIMES
alltimes = 72

ncfile = nc.Dataset(root + output)

print('Fetching variables...')
cttvar = wrf.getvar(ncfile, 'ctt', timeidx = alltimes, units = 'degC')
print('CTT fetched.')
cttlevs = np.arange(-90, 51, 1)
ticks = np.arange(-100, 61, 10)

#colors_enhanced = mcolors.ListedColormap
colorarray = np.array([[0,255,255,255],[0,242,248,255],[0,230,241,255],[0,217,234,255],[0,204,227,255],[0,191,220,255],[0,179,213,255],[0,166,206,255],[0,153,199,255],[0,140,192,255],[0,128,185,255],[0,115,178,255],[0,102,171,255],[0,89,164,255],[0,77,157,255],[0,64,150,255],[0,51,143,255],[0,38,136,255],[0,26,129,255],[0,13,122,255],[0,0,115,255],[0,0,115,255],[0,21,105,255],[0,43,96,255],[0,64,86,255],[0,85,77,255],[0,106,67,255],[0,128,58,255],[0,149,48,255],[0,170,38,255],[0,191,29,255],[0,213,19,255],[0,234,10,255],[0,255,0,255],[26,255,0,255],[51,255,0,255],[77,255,0,255],[102,255,0,255],[128,255,0,255],[153,255,0,255],[179,255,0,255],[204,255,0,255],[230,255,0,255],[255,255,0,255],[255,230,0,255],[255,204,0,255],[255,179,0,255],[255,153,0,255],[255,128,0,255],[255,102,0,255],[255,77,0,255],[255,51,0,255],[255,26,0,255],[255,0,0,255],[230,0,0,255],[204,0,0,255],[179,0,0,255],[153,0,0,255],[128,0,0,255],[102,0,0,255],[77,0,0,255],[51,0,0,255],[26,0,0,255],[0,0,0,255],[26,26,26,255],[51,51,51,255],[77,77,77,255],[102,102,102,255],[128,128,128,255],[153,153,153,255],[179,179,179,255],[204,204,204,255],[230,230,230,255],[255,127,203,255],[242,114,195,255],[229,102,188,255],[217,89,180,255],[204,76,173,255],[191,64,165,255],[178,51,157,255],[165,38,150,255],[153,25,142,255],[140,13,135,255],[127,0,127,255],[255,255,0,255],[219,219,0,255],[182,182,0,255],[146,146,0,255],[109,109,0,255],[73,73,0,255],[36,36,0,255],[0,0,0,255],[255,0,0,255],[255,26,26,255],[255,51,51,255],[255,77,77,255],[255,102,102,255],[255,128,128,255],[255,153,153,255],[255,179,179,255],[255,204,204,255],[255,230,230,255],[255,255,255,255]]) / 255.
#Colors borrowed from: https://cimss.ssec.wisc.edu/satellite-blog/wp-content/uploads/2020/07/IR4AVHRR100_colortable.txt
#Credit to Jim Nelson of CIMSS at University of Wisconsin, Madison

#colors1 = plt.cm.jet_r(np.linspace(0, .75, 128))
colors2 = plt.cm.Greys(np.linspace(.25, 1, len(colorarray)))
#colors1 = mcolors.ListedColormap(colorarray, name = 'enhancement', N = None)
colors1 = np.flip(colorarray, axis = 0)
colors = np.vstack((colors1, colors2))
cmap_enhanced = mcolors.LinearSegmentedColormap.from_list('enhanced', colors)
divnorm = mcolors.DivergingNorm(vmin = -105, vcenter = -20, vmax = 60)

def plot_ctt(cttvar, time = 0):
    fig = plt.figure(figsize = (12,8), dpi = 300)
    timevals = str(times[time])
    tim = timevals[:-10]
    ax = plt.subplot(111, projection = proj)
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=.8)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    #ctt_fconts = plt.contourf(lon, lat, cttvar, levels = cttlevs, cmap = 'Greys', transform = dataproj)
    #ctt_mesh = plt.pcolormesh(lon, lat, cttvar, cmap = 'Greys', vmin = -90, vmax = 50, transform = dataproj)
    ctt_mesh = plt.pcolormesh(lon, lat, cttvar, cmap = cmap_enhanced, norm = divnorm, transform = dataproj)
    cbar = plt.colorbar(ctt_mesh, orientation = 'vertical', ticks = ticks, pad = 0.00)
    cbar.set_label(u'Cloud Top Temperature (\N{DEGREE SIGN}C)')
    ax.set_aspect('auto')
    plt.suptitle(u'Cloud Top Temperature (\N{DEGREE SIGN}C)', fontsize = 16)
    plt.title('Valid: ' + tim + ' Z', fontsize = 12, loc = 'left')
    plt.title('Initialized: ' + init + ' Z', fontsize = 12, loc = 'right')
    plt.savefig('cttenhanced_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_ctt(cttvar, time = i)
print('Done.')
