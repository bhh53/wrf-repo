##bhh53 Summer 2020
##test animation of T2/Q2
from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc

root = '/data/bhh53/wrfout/'
output = 'wrfout_d01_2020-07-08_18:00:00'
data = xr.open_dataset(root + output)
#data.info()

lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values
#print(lat[29, 36])
#print('----------')
#print(lon[29, 36])
ithlat = 42.4534
ithlon = -76.4735
clelat = 41.47
clelon = -81.54

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)

times = xr.DataArray(data.XTIME)
inittime = str(times[0].values)
init = inittime[:-10]

temps = data.T2
T2 = xr.DataArray(temps)
#T2 = wrf.smooth2d(xr.DataArray(temps), 3, cenweight=2)
T2C = T2 - 273.15
T2F = T2C * 9/5. + 32.
#print('Max T2 is:', T2F.values.max())
#print('Min T2 is:', T2F.values.min())
hums = data.Q2
Q2 = xr.DataArray(hums)
#print(T2)
#print(Q2)
#print('Max Q2 is:', Q2.values.max())
#print('Min Q2 is:', Q2.values.min())

T2Flist = []
Q2list = []

proj = ccrs.PlateCarree()


states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
#reader = shpreader.Reader('countyl010g.shp') #obtained from: https://prd-tnm.s3.amazonaws.com/StagedProducts/Small-scale/data/Boundaries/countyl010g_shp_nt00964.tar.gz 
#counties = list(reader.geometries())
#COUNTIES = cfeature.ShapelyFeature(counties, ccrs.PlateCarree())

uwinds = data.U10
uda = xr.DataArray(uwinds)
vwinds = data.V10
vda = xr.DataArray(vwinds)

#set up plot
fig = plt.figure(figsize = (16,9), dpi = 200)

T2levs = np.arange(273, 314, 1)
T2Clevs = np.arange(0, 40, 1)
T2Flevs = np.arange(30, 111, 1)
T2Fcontlevs = np.arange(30, 120, 10) 
Q2levs = np.arange(0, .03, 4e-3) * 1e3
for i in range(len(times)):
    timevals = str(times[i].values)
    #print(timevals)
    time = timevals[:-10]
    T2Fvals = T2F[i][idx[0], idx[1]].values
    Q2vals = Q2[i][idx[0], idx[1]].values * 1e3
    ax = plt.axes(projection = proj)
    #ax.set_extent([-84,-66.8,36,47.6]) #NE
    #ax.set_extent([-120, -67, 20, 60]) #US
    #ax.set_extent([-85.5, -77.1, 39, 43.8]) #CLE 
    #ax.set_extent([-80.8,-72.4,40, 45])
    ax.set_extent([-82, -71, 38, 46])
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black')
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none')
    ax.add_feature(states, linewidth=1, edgecolor='black')
    #ax.add_feature(COUNTIES, facecolor = 'none', edgecolor = 'gray')
    T2s = ax.contourf(lon, lat, T2F[i], levels = T2Flevs, cmap = 'YlOrRd', extend = 'both')
    T2cont = ax.contour(lon, lat, T2F[i], levels = T2Fcontlevs, linewidths = 2, colors = 'black')
    T2label = plt.clabel(T2cont, fontsize = 10, inline = 1, fmt = '%2.0f')
    Q2s = ax.contour(lon, lat, Q2[i] * 1e3, levels = Q2levs, linewidths = 2, cmap = 'Greens')
    Q2label = plt.clabel(Q2s, fontsize=10, inline=1, fmt='%2.0f', colors = 'darkgreen')
    cbar = plt.colorbar(T2s, extend = 'both', ticks = [40,50,60,70,80,90,100])
    cbar.ax.set_ylabel('Temperature (' + u'\N{DEGREE SIGN}' + 'F)')
    dot = plt.scatter(lon[idx[0], idx[1]], lat[idx[0], idx[1]], s = 30)
    ax.text(0,-.01, 'Ithaca Temperature = ' + str(int(T2Fvals)) + u' \N{DEGREE SIGN}' + 'F\nIthaca Mixing Ratio = ' + str(int(Q2vals)) + ' g/kg', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'left')
    u = uda[i].values
    v = vda[i].values
    #winds = ax.quiver(lon, lat, u, v, transform = ccrs.PlateCarree())

    plt.title('Temperature (' + u'\N{DEGREE SIGN}' + 'F) and Mixing\nRatio (g/kg) at 2 Meters', fontsize = 20)
    plt.title('Initialized: ' + init + ' Z\nValid: ' + str(time) + ' Z', fontsize = 12, loc = 'right')
    plt.savefig('t2q2animtest_' + str(i) + '.png') 
    print(str(time) + ' Plot Generated.')
    plt.clf()

    ithxy = [idx[0], idx[1]]
    #T2Fvals = T2F[i][idx[0], idx[1]].values
    #Q2vals = Q2[i][idx[0], idx[1]].values
    #print('Ithaca T is:', ithT2F)
    #print('Ithaca Q is:', ithQ2)
    T2Flist.append(T2Fvals)
    Q2list.append(Q2vals)
    
print('Ithaca TMax is:', max(T2Flist))
print('Ithaca TMin is:', min(T2Flist))
print('Ithaca QMax is:', max(Q2list))






print('end')
