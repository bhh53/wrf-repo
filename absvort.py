#bhh53 Fall 2020
#Absolute Vorticity

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.cm as cm
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

idxlat = 42.4534
idxlon = -76.4735
locname = 'Ithaca'

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(idxlon, idxlat, lon, lat, 10000)
print(idx)
alltimes = 6
ustagvar = xr.DataArray(data.U[alltimes])
#ustag = ustagvar[alltimes]
vstagvar = xr.DataArray(data.V[alltimes])
#vstag = vstagvar[alltimes]
msfuvar = xr.DataArray(data.MAPFAC_U[alltimes])
#msfu = msfuvar[alltimes]
msfvvar = xr.DataArray(data.MAPFAC_V[alltimes])
#msfv = msfvvar[alltimes]
msfmvar = xr.DataArray(data.MAPFAC_MX[alltimes])
#msfm = msfmvar[alltimes]
corvar = xr.DataArray(data.F[alltimes])
#cor = corvar[alltimes]
dx = 9000.0
dy = 9000.0
ncfile = nc.Dataset(root + output)
pvar = wrf.getvar(ncfile, 'pressure', timeidx = alltimes)
zvar = wrf.getvar(ncfile, 'z', units = 'dm', timeidx = alltimes)
print('All Variables fetched.')

avo = wrf.avo(ustagvar, vstagvar, msfuvar, msfvvar, msfmvar, corvar, dx, dy)
#print(avo)
print('Full Absolute Vorticity Computed.')

z_850 = wrf.interplevel(zvar, pvar, 850)
zs_850 = ndimage.gaussian_filter(z_850, sigma = 3)
avo_850 = wrf.interplevel(avo, pvar, 850)
z_700 = wrf.interplevel(zvar, pvar, 700)
zs_700 = ndimage.gaussian_filter(z_700, sigma = 3)
avo_700 = wrf.interplevel(avo, pvar, 700)
z_500 = wrf.interplevel(zvar, pvar, 500)
zs_500 = ndimage.gaussian_filter(z_500, sigma = 3)
avo_500 = wrf.interplevel(avo, pvar, 500)
print('Completed Interpolations, Beginning Plotting...')

avolevs = np.arange(8, 60, 1)
avocmap_data = cm.YlOrBr(np.linspace(0.2, 1, 256))
avocmap = mcolors.LinearSegmentedColormap.from_list('AbsVort', avocmap_data)
avonorm = mcolors.Normalize(vmin = 8., vmax = 60.)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")

def plot_background(ax):
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=.8)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    return ax

def plot_avort_500(avo_500, zs_500, time = 6):
    fig = plt.figure(figsize = (12,8), dpi = 300)
    timevals = str(times[time])
    tim = timevals[:-10]
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    ax = plt.subplot(111, projection = proj)
    plot_background(ax)
    zlevs = np.arange(300, 900, 6)
    avo_fconts = plt.contourf(lon, lat, avo_500, levels = avolevs, cmap = avocmap, norm = avonorm, transform = dataproj, extend = 'max')
    cbar = plt.colorbar(avo_fconts, orientation = 'vertical', ticks = np.arange(8,61,4), pad = 0.00)
    cbar.set_label('Absolute Vorticity ($10^{-5} s^{-1}$)')
    ax.set_aspect('auto')
    zconts = plt.contour(lon, lat, zs_500, levels = zlevs, colors = 'k', linewidths = 1, transform = dataproj)
    zlabel = plt.clabel(zconts, fontsize = 8, inline = 1, fmt = '%i')
    plt.title('500 mb Height (dam),\nAbsolute Vorticity ($10^{-5} s^{-1}$)', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + str(tim) + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('avort_500_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_avort_500(avo_500, zs_500, time = i)
print('500 Abs Vort Plot Generated.')

def plot_avort_700(avo_700, zs_700, time = 6):
    fig = plt.figure(figsize = (12,8), dpi = 300)
    timevals = str(times[time])
    tim = timevals[:-10]
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    ax = plt.subplot(111, projection = proj)
    plot_background(ax)
    zlevs = np.arange(0, 345, 3)
    avo_fconts = plt.contourf(lon, lat, avo_700, levels = avolevs, cmap = avocmap, norm = avonorm, transform = dataproj, extend = 'max')
    cbar = plt.colorbar(avo_fconts, orientation = 'vertical', ticks = np.arange(8,61,4), pad = 0.00)
    cbar.set_label('Absolute Vorticity ($10^{-5} s^{-1}$)')
    ax.set_aspect('auto')
    zconts = plt.contour(lon, lat, zs_700, levels = zlevs, colors = 'k', linewidths = 1, transform = dataproj)
    zlabel = plt.clabel(zconts, fontsize = 8, inline = 1, fmt = '%i')
    plt.title('700 mb Height (dam),\nAbsolute Vorticity ($10^{-5} s^{-1}$)', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + str(tim) + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('avort_700_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_avort_700(avo_700, zs_700, time = i)
print('700 Abs Vort Plot Generated.')

def plot_avort_850(avo_850, zs_850, time = 6):
    fig = plt.figure(figsize = (12,8), dpi = 300)
    timevals = str(times[time])
    tim = timevals[:-10]
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    ax = plt.subplot(111, projection = proj)
    plot_background(ax)
    zlevs = np.arange(0, 345, 3)
    avo_fconts = plt.contourf(lon, lat, avo_850, levels = avolevs, cmap = avocmap, norm = avonorm, transform = dataproj, extend = 'max')
    cbar = plt.colorbar(avo_fconts, orientation = 'vertical', ticks = np.arange(8,61,4), pad = 0.00)
    cbar.set_label('Absolute Vorticity ($10^{-5} s^{-1}$)')
    ax.set_aspect('auto')
    zconts = plt.contour(lon, lat, zs_850, levels = zlevs, colors = 'k', linewidths = 1, transform = dataproj)
    zlabel = plt.clabel(zconts, fontsize = 8, inline = 1, fmt = '%i')
    plt.title('850 mb Height (dam),\nAbsolute Vorticity ($10^{-5} s^{-1}$)', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + str(tim) + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('avort_850_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_avort_850(avo_850, zs_850, time = i)
print('850 Abs Vort Plot Generated.')


print('Done.')

