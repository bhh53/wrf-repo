#bhh53 Fall 2020
#6-Hour QPF

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

idxlat = 42.4534
idxlon = -76.4735
locname = 'Ithaca'

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(idxlon, idxlat, lon, lat, 10000)
print(idx)


rainc = data.RAINC
rainnc = data.RAINNC
cda = xr.DataArray(rainc)
ncda = xr.DataArray(rainnc)
precipmm = cda.values + ncda.values
precip = precipmm * 0.0393701 #Convert to Inches
#smoothpre = ndimage.gaussianfilter(precip, sigma = 3)
preciplevs = [0.00, 0.01, 0.10, 0.25, 0.5, 0.75, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 6.0, 8.0, 10.0]
#cmap_data_int = np.array([(0,0,0),(20, 200, 250),(61, 133, 198),(0,20,250),(0,250,20),(0,160,15),(0,100,10),(250,250,0),(255, 217,102),(250,150,0),(250,0,0),(175,0,0),(125,0,0),(250,0,250),(125,75,255)])
#cmap_data = cmap_data_int / 255.
#print(cmap_data)
cmap_data = [(1.0,1.0, 1.0),(0.07843137, 0.78431373, 0.98039216),(0.23921569, 0.52156863, 0.77647059),(0., 0.07843137, 0.98039216),(0., 0.98039216, 0.07843137),(0., 0.62745098, 0.05882353),(0., 0.39215686, 0.03921569),(0.98039216, 0.98039216, 0.),(1., 0.85098039, 0.4),(0.98039216, 0.58823529, 0.),(0.98039216, 0., 0.),(0.68627451, 0., 0.),(0.49019608, 0., 0.),(0.98039216, 0., 0.98039216),(0.49019608, 0.29411765, 1.),(0.8627451, 0.8627451, 0.8627451)]
cmap = mcolors.ListedColormap(cmap_data, 'QPF')
norm = mcolors.BoundaryNorm(preciplevs, 16)
states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")

def plot_QPF_6hr(precip, time = 6):
    fig = plt.figure(figsize = (12,8), dpi = 300)
    timevals = str(times[time])
    tim = timevals[:-10]
    subtime = time - 6
    precip_6hr = precip[time] - precip[subtime]
    idxprecip = precip_6hr[idx[0], idx[1]]
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    ax = plt.subplot(111, projection = proj)
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=1)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    precips = ax.contourf(lon, lat, precip_6hr, levels = preciplevs, cmap = cmap, norm = norm, transform = dataproj, extend = 'neither')
    cbar = plt.colorbar(precips, orientation = 'vertical', ticks = preciplevs, pad = 0.00)
    cbar.set_label('Accumulated Precipitation (in.)')
    ax.set_aspect('auto')
    ax.text(0,-.01, locname + ' Rain Total: ' + str(format(idxprecip, '.2f')) + ' in.', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'left')
    ax.text(1,-.01, 'Max Rain Total: ' + str(format(precip_6hr.max(), '.2f')) + ' in.', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'right')
    plt.title('6-Hour Accumulated Precipitation', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + tim + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('qpf6_amd_' + init + '_' + str(time) + '.png')
    #print('Precip Plot ' + time + 'Generated.')
    #plt.clf()
    return ax
#print(range(0, len(times), 6))
for i in range(6, len(times), 6):
    plot_QPF_6hr(precip, time = i)
    print('QPF Plot Hr ' + str(i) + ' Generated.')
print('Done.')





