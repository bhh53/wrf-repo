#bhh53 Winter 2021 Temperature Plot
#Temperature at Pressure Levels

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.colors as mcolors
#import matplotlib.cm as cm
#import metpy.plots.ctables as ctables
#import metpy.calc as mcalc
import cartopy.crs as ccrs
import cartopy.feature as cfeature
#import cartopy.mpl.ticker as cticker
#import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
#import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()

times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

####################
idxlat = 42.4534
idxlon = -76.4735
####################

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(idxlon, idxlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
dataproj = ccrs.PlateCarree()

#alltimes = wrf.ALL_TIMES
alltimes = 72

ncfile = nc.Dataset(root + output)

print('Fetching variables...')
tcvar = wrf.getvar(ncfile, 'tc', timeidx = alltimes)
print('Temperature fetched.')
pvar = wrf.getvar(ncfile, 'pressure', timeidx = alltimes)
print('Pressure fetched.')
uvar = wrf.getvar(ncfile, 'ua', units = 'kt', timeidx = alltimes)
print('U fetched.')
vvar = wrf.getvar(ncfile, 'va', units = 'kt', timeidx = alltimes)
print('V fetched.')

print('Interpolating...')
plevs = [1000, 925, 850, 700, 500, 300, 200, 100, 50, 10]
tc_zs = wrf.vinterp(ncfile, tcvar, 'p', plevs, extrapolate = True, timeidx = alltimes)
u_zs = wrf.vinterp(ncfile, uvar, 'p', plevs, extrapolate = True, timeidx = alltimes)
v_zs = wrf.vinterp(ncfile, vvar, 'p', plevs, extrapolate = True, timeidx = alltimes)
tclevs = np.arange(-40, 35, 2)
tcticks = np.arange(-40, 45, 5)
print('Beginning Plotting...')
def plot_background(ax):
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=.8)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    return ax
def plot_tc_uv(tc_zs, u_zs, v_zs, plevidx = 0, time = 0):
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    timevals = str(times[time])
    tim = timevals[:-10]
    fig = plt.figure(figsize = (12,8), dpi = 300)
    ax = plt.subplot(111, projection = proj)
    plot_background(ax)
    tc_fconts = plt.contourf(lon, lat, tc_zs[plevidx], levels = tclevs, cmap = 'gist_rainbow_r', extend = 'both', transform = dataproj)
    #barbs = plt.barbs(lon[::20, ::20], lat[::20, ::20], u_zs[plevidx][::20, ::20], v_zs[plevidx][::20, ::20], length = 5, colors = 'darkgrey', transform = dataproj)
    cbar = plt.colorbar(tc_fconts, orientation = 'vertical', ticks = tcticks, pad = 0.00)
    cbar.set_label('Temperature (\N{DEGREE SIGN}C)')
    ax.set_aspect('auto')
    plt.suptitle('Temperature (\N{DEGREE SIGN}C) and Winds (kt) at ' + str(plevs[plevidx]) + ' hPa', fontsize = 14)
    plt.title('Valid: ' + tim + ' Z', fontsize = 12, loc = 'left')
    plt.title('Initialized: ' + init + ' Z', fontsize = 12, loc = 'right')
    plt.savefig('t_' + str(plevs[plevidx]) + '_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_tc_uv(tc_zs, u_zs, v_zs, plevidx = 3, time = i)
print('Temp plot ' + str(i) + 'Generated.')

print('Done.')
