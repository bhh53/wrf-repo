#bhh53 Summer 2020
from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.cm import get_cmap
from matplotlib.font_manager import FontProperties
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc


root = '/data/bhh53/wrfout/'
output = 'wrfout_d01_2020-07-14_12:00:00'
data = xr.open_dataset(root + output)
#data.info()

times = xr.DataArray(data.XTIME)
inittime = str(times[0].values)
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values


ithlat = 42.4534
ithlon = -76.4735
def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)
states = cfeature.NaturalEarthFeature(category="cultural", scale="10m", facecolor="none", name="admin_1_states_provinces_shp")
alltimes = wrf.ALL_TIMES
ncfile = nc.Dataset(root + output)
pvar = wrf.getvar(ncfile, 'pressure')
uavar = wrf.getvar(ncfile, 'ua', units = 'kt')
vavar = wrf.getvar(ncfile, 'va', units = 'kt')
wspdvar = wrf.getvar(ncfile, 'wspd_wdir', units='kts')[0,:]
wdirvar = wrf.getvar(ncfile, 'wspd_wdir', units = 'kts')[1,:]
rhvar = wrf.getvar(ncfile, 'rh')
p = xr.DataArray(pvar)
u = xr.DataArray(uavar)
v = xr.DataArray(vavar)
wspd = xr.DataArray(wspdvar)
print(wspd.attrs)
wdir = xr.DataArray(wdirvar)
print(wdir)
rh = xr.DataArray(rhvar)
print(rh)
def plot_850_rh_winds(p, u, v, wspd, wdir, rh, time = 0, plevel = 850):
    fig = plt.figure(figsize = (16,10), dpi = 200)
    ax = plt.axes(projection = ccrs.PlateCarree())
    timeval = str(times[time].values)
    tim = timeval[:-10]
    u_z = wrf.interplevel(u, p, plevel).values
    v_z = wrf.interplevel(v, p, plevel).values
    rh_z = wrf.interplevel(rh, p, plevel).values
    wspd_z = wrf.interplevel(wspd, p, plevel)
    wdir_z = wrf.interplevel(wdir, p, plevel)
    rhvals = rh_z[idx[0], idx[1]]
    wspdvals = wspd_z[idx[0], idx[1]]
    wdirvals = wdir_z[idx[0], idx[1]]
    #ax.set_extent([-123, -67, 21, 55]) #US
    ax.set_extent([-80.8, -71, 38, 46])
    ax.add_feature(cfeature.COASTLINE.with_scale('10m'), edgecolor = 'black')
    ax.add_feature(cfeature.LAKES.with_scale('10m'), edgecolor = 'black', facecolor = 'none') #cfeature.COLORS['water'])
    ax.add_feature(cfeature.RIVERS.with_scale('10m'), edgecolor = cfeature.COLORS['water'])
    ax.add_feature(states, linewidth=1, edgecolor='black')
    rhlevs = np.arange(0, 105, 5)
    rhconts = plt.contourf(lon, lat, rh_z, levels = rhlevs, cmap = 'YlGnBu')
    quivers = plt.quiver(lon[::5, ::5], lat[::5, ::5], u_z[::5, ::5], v_z[::5, ::5])
    plt.quiverkey(quivers, 0.03, 1.01, 10, label = '10 kt', coordinates = 'axes', labelpos = 'E') #fontproperties = {family=sans-serif, style=normal, variant=normal, stretch=normal, weight=normal, size=12})
    cbar = plt.colorbar(rhconts, orientation = 'vertical', ticks = rhlevs)
    cbar.set_label('Relatove Humidity (%)')
    #plt.clabel(rhconts, fontsize = 14, inline = 1, fmt = '%2i')
    dot = plt.scatter(lon[idx[0], idx[1]], lat[idx[0], idx[1]], s = 30, color = 'orange')
    ax.text(0,-.01, 'Ithaca RH: ' + str(int(rhvals)) + '%\nIthaca Wind: ' + str(int(wspdvals)) + ' kt @ ' + str(int(wdirvals)) + ' degrees', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'left')
    ax.text(1,-.01, 'Max Wind Speed: ' + str(int(wspdvals.max())) + ' kt', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'right')
    plt.title('RH (%) and Winds @ ' + str(plevel) + ' mb', fontsize = 16)
    plt.title('Initialized: ' + init + ' Z\nValid: ' + str(tim) + ' Z', fontsize = 12, loc = 'right')
    plt.savefig('rh_' + str(plevel) + '_' + str(time) + '.png')
    return ax

plot_850_rh_winds(p, u, v, wspd, wdir, rh, time = 0, plevel = 850)
print('Plot Generated.')






