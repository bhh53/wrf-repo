#bhh53 Summer 2020
#Time Series for Temperature

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc

root = '/data/bhh53/wrfout/'
output = 'wrfout_d03_2020-09-13_12:00:00'
data = xr.open_dataset(root + output)
#data.info()

lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values
ithlat = 42.4534
ithlon = -76.4735
mialat = 25.79
mialon = -80.32

#locname = 'Ithaca'
locname = 'Miami'

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)

times = xr.DataArray(data.XTIME)
inittime = str(times[0].values)
init = inittime[:-10]


temps = data.T2
T2 = xr.DataArray(temps)
#T2 = wrf.smooth2d(xr.DataArray(temps), 3, cenweight=2)
T2C = T2 - 273.15
T2F = T2C * 9/5. + 32.
T2Flist = []

rainc = data.RAINC
rainnc = data.RAINNC
cda = xr.DataArray(rainc)
ncda = xr.DataArray(rainnc)
precip = cda.values + ncda.values
#smoothpre = wrf.smooth2d(precip, 4, cenweight=2)
preciplist = []

alltimes = wrf.ALL_TIMES
ncfile = nc.Dataset(root + output)
pvar = wrf.getvar(ncfile, 'pressure', timeidx = alltimes)
print('Pressure Variable fetched.')
#rhvar = wrf.getvar(ncfile, 'rh', timeidx = alltimes)
print('All Variables fetched.')

p = xr.DataArray(pvar).values
#rh = xr.DataArray(rhvar).values
QVAPOR = data.QVAPOR
qv = xr.DataArray(QVAPOR)
tdF = wrf.td(p, qv, units = 'degF')
tdFsfc = tdF[:, 0]
#print(tdFsfc)
tdFlist = []

timelist = times.values
#plevel = 500
#rh_z = wrf.interplevel(rh, p, plevel)
hourly_precip = []

for i in range(len(times)):
    timevals = str(times[i].values)
    #print(timevals)
    time = timevals[:-10]
    ptT2F = T2F[i][idx[0], idx[1]].values #T2 at certain gridpoint idx
    T2Flist.append(ptT2F)
    pttdF = tdFsfc[i][idx[0], idx[1]].values #Td
    tdFlist.append(pttdF)
#for i in range(len(times) - 2):
#    hourly_precip[i + 1] = precip[i + 1] - precip[i]
#    ptprecip = hourly_precip[i][idx[0], idx[1]]
#    preciplist.append(ptprecip)

#print(T2Flist, tdFlist)#, preciplist)
def plot_timeseries_T_Precip(timelist, T2Flist, tdFlist):
    fig = plt.figure(figsize = (10,10), dpi = 200)
    #ax = fig.add_subplot(111)
    axT = fig.add_subplot(211)
    axP = fig.add_subplot(212)
    Tplot = axT.plot(timelist, T2Flist, color = 'red', lw = 2)
    Tdplot = axT.plot(timelist, tdFlist, color = 'blue', lw = 2)
    axT.set_title(locname + ' Temperature and Dew Point Forecast', loc = 'center')
    axP.set_title(locname + ' Precipitation Forecast', loc = 'center')
    axT.set_ylabel('2-Meter Temperature and Dew Point (' + u'\N{DEGREE SIGN}' + 'F)')
    axP.set_ylabel('Hourly Precipitation Total (mm)')
    plt.savefig('timeseries_' + locname + '_' + init + '.png')
    print(locname + ' time series generated')

plot_timeseries_T_Precip(timelist, T2Flist, tdFlist)
print('Done.')
