#bhh53 Fall 2020
#Vertical Velocity CONUS plot

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.cm import get_cmap
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values


ithlat = 42.4534
ithlon = -76.4735

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
print('Fetching variables...')
#alltimes = wrf.ALL_TIMES
alltimes = 42

ncfile = nc.Dataset(root + output)
pvar = wrf.getvar(ncfile, 'pressure', timeidx = alltimes)
print('Pressure fetched.')
zvar = wrf.getvar(ncfile, 'z', units = 'dm', timeidx = alltimes)
print('Z fetched')
wavar = wrf.getvar(ncfile, 'wa', units = 'm s-1', timeidx = alltimes)
print('W fetched.')

p = pvar.values
z = zvar.values
w = wavar.values
print('Arrays Ready.')

####################
## Pressure Level ##
plevel = 700
####################

ht_z = wrf.interplevel(z, p, plevel).values
ht_smooth = ndimage.gaussian_filter(ht_z, sigma = 3)
w_z = wrf.interplevel(w, p, plevel)

print('Completed interpolations. Beginning Plots.')
print('W max = ',np.nanmax(w_z))
print('W min = ',np.nanmin(w_z))

def plot_w_z(u_z, v_z, w_z, ht_smooth, plevel = plevel, time = 0):
    fig = plt.figure(figsize = (12,8), dpi = 300)
    timevals = str(times[time])
    tim = timevals[:-10]
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    ax = plt.subplot(111, projection = proj)
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth=.8)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none', linewidth=.5)
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    if plevel <= 300:
        zlevs = np.arange(780, 1680, 12)
    elif 300 < plevel <= 500:
        zlevs = np.arange(498, 780, 6)
    elif plevel > 500:
        zlevs = np.arange(0, 345, 3)
    wlevs = np.arange(-3, 3.08, .08)
    w_conts = plt.contourf(lon, lat, w_z, levels = wlevs, cmap = 'seismic_r', extend = 'both', transform = dataproj)
    cbar = plt.colorbar(w_conts, ax=ax, orientation = 'vertical', extend = 'both', ticks = wlevs[::5], pad = 0.01)
    cbar.set_label('Vertical Velocity (m/s)\nBlue = Ascent')
    ax.set_aspect('auto')
    zconts = plt.contour(lon, lat, ht_smooth, levels = zlevs, colors = 'k', transform = dataproj, linewidths = 1)
    zlabel = plt.clabel(zconts, fontsize = 8, inline = 1, fmt = '%i')
    plt.title(str(plevel) + ' mb Height (dam),\nVertical Velocity (m/s)', fontsize = 14)
    plt.title('Init: ' + init + ' Z\nValid: ' + str(tim) + ' Z', fontsize = 10, loc = 'right')
    plt.savefig('vertvel_' + str(plevel) + '_amd_' + init + '_' + str(time) + '.png')
    plt.clf()
    return ax

i = alltimes
plot_w_z(u_z, v_z, w_z, ht_smooth, plevel = plevel, time = i)
print('Plot ' + str(i) + ' Generated.')

print('Done.')

