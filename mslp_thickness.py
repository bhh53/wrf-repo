#bhh53 Summer 2020
#Winds Plot loop

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.cm import get_cmap
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc

root = '/data/bhh53/wrfout/'
output = 'wrfout_d01_2020-07-14_12:00:00'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME)
inittime = str(times[0].values)
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values


ithlat = 42.4534
ithlon = -76.4735

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]

idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 10000)
print(idx)





states = cfeature.NaturalEarthFeature(category="cultural", scale="10m", facecolor="none", name="admin_1_states_provinces_shp")


ncfile = nc.Dataset(root + output)
pvar = wrf.getvar(ncfile, 'pressure')
z = wrf.getvar(ncfile, 'z', units = 'm')
slp = wrf.getvar(ncfile, 'slp', units = 'hPa')
tc = wrf.getvar(ncfile, 'temp', units = 'degC')
tf = wrf.getvar(ncfile, 'temp', units = 'degF')
pda = xr.DataArray(pvar)
slpda = xr.DataArray(slp).values

def plot_slp_thick_t(time = 0):
    fig = plt.figure(figsize = (20,12), dpi = 200)
    ax = plt.axes(projection = ccrs.PlateCarree())
    timeval = str(times[time].values)
    tim = timeval[:-10]
    ht1000 = wrf.interplevel(z, pda, 1000).values
    ht500 = wrf.interplevel(z, pda, 500).values
    tc850 = wrf.interplevel(z, pda, 850).values
    thick = ht500 - ht1000
    slplevs = np.arange(960, 1064, 2)
    tclevs = np.arange(-30, 50, 5)
    thicklevs = np.arange(5280, 6060, 30)
    ax.set_extent([-84, -66.8, 36, 47.6])
    ax.add_feature(cfeature.COASTLINE.with_scale('10m'), edgecolor = 'black')
    ax.add_feature(cfeature.LAKES.with_scale('10m'), edgecolor = 'black', facecolor = cfeature.COLORS['water'])
    ax.add_feature(cfeature.RIVERS.with_scale('10m'), edgecolor = cfeature.COLORS['water'])
    ax.add_feature(states, linewidth=1, edgecolor='black')
    thickconts = plt.contour(lon, lat, thick, levels = thicklevs, colors = 'blue', linewidths = 2, transform = ccrs.PlateCarree())
    thicklabels = plt.clabel(thickconts, fontsize = 12, inline = 1, fmt = '%4i', colors = 'blue')
    slpconts = plt.contour(lon, lat, slp, levels = slplevs, colors = 'green', linestyles = 'dashed', linewidths = 2, transform = ccrs. PlateCarree())
    slplabels = plt.clabel(slpconts, fontsize = 12, inline = 1, fmt = '%4i', colors = 'green')
    plt.title('1000-500mb Thickness (m)\nand Sea Level Pressure (hPa)')
    plt.savefig('thicktest.png')
    return ax

plot_slp_thick_t()
print('Plot Generated.')
