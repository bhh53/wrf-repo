##Blake Himes bhh53
##Fall 2020
##Plot dBZ & MSLP

from datetime import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import metpy.plots.ctables as ctables
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cticker
import cartopy.io.shapereader as shpreader
import wrf
import netCDF4 as nc
import scipy.ndimage as ndimage

root = '/data/bhh53/wrf-test-data/'
output = 'wrfout_d01_amd.nc'
data = xr.open_dataset(root + output)
#data.info()


times = xr.DataArray(data.XTIME).values
inittime = str(times[0])
init = inittime[:-10]
lats = xr.DataArray(data.XLAT)
lat = lats[0].values
lons = xr.DataArray(data.XLONG)
lon = lons[0].values

ithlat = 42.4534
ithlon = -76.4735

def indexOfClosestNode(target_lon, target_lat, lon_grid, lat_grid, radius):
    indexes = np.where( (lon_grid >= (target_lon - radius)) &
                        (lon_grid <= (target_lon + radius)) &
                        (lat_grid >= (target_lat - radius)) &
                        (lat_grid <= (target_lat + radius)) )
    lon_diffs = lon_grid[indexes] - target_lon
    lat_diffs = lat_grid[indexes] - target_lat
    distances = np.sqrt((lon_diffs ** 2) + (lat_diffs ** 2))
    indx = np.where(distances == distances.min())[0][0]
    return indexes[0][indx], indexes[1][indx]
idx = indexOfClosestNode(ithlon, ithlat, lon, lat, 9000)
print(idx)

#alltimes = wrf.ALL_TIMES
alltimes = 60

ncfile = nc.Dataset(root + output)
pvar = wrf.getvar(ncfile, 'pressure', timeidx = alltimes)
print('Pressure Variable fetched.')
slpvar = wrf.getvar(ncfile, 'slp', timeidx = alltimes)
print('SLP Variable fetched.')
slp_smooth = ndimage.gaussian_filter(slpvar, sigma = 3)
#print(slpvar)
zvar = wrf.getvar(ncfile, 'z', timeidx = alltimes)
print('Z Variable fetched.')
dbzvar = wrf.getvar(ncfile, 'mdbz', timeidx = alltimes)
print('dBz Variable fetched.')
#print(dbzvar)
#tkvar = wrf.getvar(ncfile, 'tk', timeidx = alltimes)
#print('TK Variable fetched.')

states = cfeature.NaturalEarthFeature(category="cultural", scale="50m", facecolor="none", name="admin_1_states_provinces_shp")
cmap_data = [(1.0, 1.0, 1.0), (0.0, 0.9254901960784314, 0.9254901960784314), (0.00392156862745098, 0.6274509803921569, 0.9647058823529412), (0.0, 0.0, 0.9647058823529412), (0.0, 1.0, 0.0), (0.0, 0.7843137254901961, 0.0), (0.0, 0.5647058823529412, 0.0), (1.0, 1.0, 0.0), (0.9058823529411765, 0.7529411764705882, 0.0), (1.0, 0.5647058823529412, 0.0), (1.0, 0.0, 0.0), (0.8392156862745098, 0.0, 0.0), (0.7529411764705882, 0.0, 0.0), (1.0, 0.0, 1.0), (0.6, 0.3333333333333333, 0.788235294117647), (0.0, 0.0, 0.0)]
dbzlevs = np.arange(-5, 80, 5)
cmap = mcolors.ListedColormap(cmap_data)
#norm = mcolors.BoundaryNorm(dbzlevs, 23)
norm = mcolors.Normalize(0, 80)
'''
#fig = plt.figure(figsize = (12,8), dpi = 100)
proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=16)
ax = plt.subplot(111, projection = proj)
def plot_maxmin_points(lon, lat, data, extrema, nsize, symbol, color='k',
                       plotValue=True, transform=None):
    from scipy.ndimage.filters import maximum_filter, minimum_filter

    if (extrema == 'max'):
        data_ext = maximum_filter(data, nsize, mode='nearest')
    elif (extrema == 'min'):
        data_ext = minimum_filter(data, nsize, mode='nearest')
    else:
        raise ValueError('Value for hilo must be either max or min')

    mxy, mxx = np.where(data_ext == data)
    print('mxy = ',mxy)
    print('mxx = ',mxx)

    for i in range(len(mxy)):
        ax.text(lon[mxy[i], mxx[i]], lat[mxy[i], mxx[i]], symbol, color=color, size=24,
                clip_on=True, horizontalalignment='center', verticalalignment='center',
                transform=transform)
        ax.text(lon[mxy[i], mxx[i]], lat[mxy[i], mxx[i]],
                '\n' + str(np.int(data[mxy[i], mxx[i]])),
                color=color, size=12, clip_on=True, fontweight='bold',
                horizontalalignment='center', verticalalignment='top', transform=transform)

#fig = plt.figure(figsize = (24,16), dpi = 200)
#proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=16)
#ax = plt.subplot(111, projection = proj)
'''
def plot_dbz(dbzvar, slpvar, time = 0):
    fig = plt.figure(figsize = (12,8), dpi = 200)
    timevals = str(times[time])
    tim = timevals[:-10]
    #dbzvals = dbzvar[i][idx[0], idx[1]]
    idxdbz = dbzvar[idx[0], idx[1]]
    proj = ccrs.LambertConformal(central_longitude=-89.46, central_latitude=42.49, standard_parallels=(30,60), cutoff=10)
    dataproj = ccrs.PlateCarree()
    ax = plt.subplot(111, projection = proj)
    ax.set_extent([-124, -60, 16, 62]) #CONUS
    ax.add_feature(cfeature.COASTLINE.with_scale('50m'), edgecolor = 'black', linewidth = 0.75)
    ax.add_feature(cfeature.LAKES.with_scale('50m'), edgecolor = 'black', facecolor = 'none')
    #ax.add_feature(cfeature.RIVERS.with_scale('10m'), edgecolor = cfeature.COLORS['water'])
    ax.add_feature(states, linewidth=.5, edgecolor='black')
    slplevs = np.arange(900, 1100, 4)
    slps = plt.contour(lon, lat, slp_smooth, colors = 'darkgrey', linewidths = 1, levels = slplevs, transform = dataproj)
    slplabel = plt.clabel(slps, fontsize = 10, inline = 1, fmt = '%4.0f', colors = 'black')

    def plot_maxmin_points(lon, lat, data, extrema, nsize, symbol, color='k',
                           plotValue=True, transform=None):
        from scipy.ndimage.filters import maximum_filter, minimum_filter
    
        if (extrema == 'max'):
            data_ext = maximum_filter(data, nsize, mode='nearest')
        elif (extrema == 'min'):
            data_ext = minimum_filter(data, nsize, mode='nearest')
        else:
            raise ValueError('Value for hilo must be either max or min')
    
        mxy, mxx = np.where(data_ext == data)
        #print('mxy = ',mxy)
        #print('mxx = ',mxx)
    
        for i in range(len(mxy)):
            ax.text(lon[mxy[i], mxx[i]], lat[mxy[i], mxx[i]], symbol, color=color, size=24, clip_on=True, horizontalalignment='center', verticalalignment='center', transform=transform)
            ax.text(lon[mxy[i], mxx[i]], lat[mxy[i], mxx[i]], '\n' + str(np.int(data[mxy[i], mxx[i]])), color=color, size=12, clip_on=True, fontweight='bold', horizontalalignment='center', verticalalignment='top', transform=transform)
    plot_maxmin_points(lon, lat, slpvar, 'max', 150, symbol='H', color='b', transform = dataproj)
    plot_maxmin_points(lon, lat, slpvar, 'min', 150, symbol='L', color='r', transform = dataproj)
    dbzs = plt.pcolormesh(lon, lat, dbzvar, cmap = cmap, norm = norm, transform = dataproj)
    cbar = plt.colorbar(dbzs, orientation = 'vertical', ticks = dbzlevs, pad = 0.00)
    cbar.set_label('Base Reflectivity (dBz)')
    ax.set_aspect('auto')
    ax.text(0,-.01, 'Ithaca Reflectivity: ' + str(int(idxdbz)) + ' dBz', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'left')
    ax.text(1,-.01, 'Max Reflectivity: ' + str(int(dbzvar.max())) + ' dBz', transform = ax.transAxes, fontsize = 12, verticalalignment = 'top', horizontalalignment = 'right')
    #dot = plt.scatter(lon[idx[0], idx[1]], lat[idx[0], idx[1]], s = 10)
    plt.suptitle('Simulated Reflectivity (dBz) and SLP (hPa)', fontsize = 16)
    plt.title('Valid: ' + tim + ' Z', fontsize = 12, loc = 'left')
    plt.title('Initialized: ' + init + ' Z', fontsize = 12, loc = 'right')
    plt.savefig('/home/bhh53/Documents/dbz_slp_d01_amd_' + init + '_' + str(time) + '.png')
    #plt.savefig('dbz_slp_d01_amd_' + init + '_' + str(time) + '.png')
    #plt.clf()

    return ax

#for i in range(len(times)):
i = alltimes
plot_dbz(dbzvar, slpvar, time = i)
print('dBz Plot ' + str(i) + ' Generated.')

print('Done.')
